import React from "react";

import { Popover } from "../../../components/common/Popover";
import { Header } from "../../../components/Header";

const Layout: React.FC = ({ children }) => (
  <div>
    <Header />
    <Popover width={100} top={75} requestClose>
      {{
        body: <div>pop over body</div>,
        toggleButton: <button>click to toggle</button>,
      }}
    </Popover>
    <div>{children}</div>
  </div>
);

export { Layout };
