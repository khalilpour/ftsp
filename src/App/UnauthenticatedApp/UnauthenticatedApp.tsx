import React from "react";
import { TestScreen } from "../../screens/TestScreen";

import { Layout } from "./Layout";

const UnauthenticatedApp: React.FC = () => (
  <Layout>
    <TestScreen />
  </Layout>
);

export { UnauthenticatedApp };
