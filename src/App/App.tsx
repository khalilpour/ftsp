import React from "react";

import { UnauthenticatedApp } from "./UnauthenticatedApp";

const App: React.FC = () => {
  const loggedIn = false;

  return loggedIn ? <div>authenticated app</div> : <UnauthenticatedApp />;
};

export { App };
