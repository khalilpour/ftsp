import React from "react";

const TestScreen: React.FC = () => (
  <p>
    {/* create .env.local file in project root dir. add this line to it "REACT_APP_API_URL=url" */}
    unauthenticated app. read this from env: {process.env.REACT_APP_API_URL}
  </p>
);

export { TestScreen };
