import React from "react";

import logo from "../../assets/images/logo.svg";
import { Logo } from "./styles";

const Header: React.FC = () => (
  <header>
    <Logo src={logo} alt="logo" />
    this is header
  </header>
);

export { Header };
