import React from "react";
import { Backdrop as BackdropStyled } from "./styles";

interface IPropTypes {
  onClick: () => void;
}

const Backdrop: React.FC<IPropTypes> = ({ onClick }) => {
  return <BackdropStyled onClick={() => onClick()}></BackdropStyled>;
};

export { Backdrop };
