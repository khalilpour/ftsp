import React, { useState, ReactNode } from "react";
import { useTransition } from "react-spring";

import { Backdrop } from "../Backdrop";
import { PopoverBody } from "./PopoverBody/PopoverBody";
import { ToggleWrapper } from "./styles";

interface IPropTypes {
  requestClose?: boolean;
  children: {
    body: ReactNode;
    toggleButton: ReactNode;
  };
  top: number;
  width: number;
}

export const Popover: React.FC<IPropTypes> = ({
  children: { toggleButton, body },
  requestClose,
  width,
  top,
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const transition = useTransition(isOpen, null, {
    from: { opacity: 0, transform: "translateY(10px)" },
    enter: { opacity: 1, transform: "translateY(0px)" },
    leave: { opacity: 0, transform: "translateY(10px)" },
  });

  return (
    <>
      {/* toggle button section */}
      <ToggleWrapper onClick={() => setIsOpen(!isOpen)}>
        {toggleButton}
      </ToggleWrapper>

      {/* popover body with animation */}
      {transition.map(
        ({ item, key, props: animateStyles }) =>
          item && (
            <PopoverBody
              key={key}
              style={animateStyles}
              width={width}
              top={top}
            >
              {body}
            </PopoverBody>
          )
      )}

      {/* Backdrop section */}
      {requestClose && isOpen ? (
        <Backdrop onClick={() => setIsOpen(false)} />
      ) : null}
    </>
  );
};
