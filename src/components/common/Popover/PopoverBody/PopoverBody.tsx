import styled from "styled-components";
import { animated } from "react-spring";

interface IPropTypes {
  width: number;
  top: number;
  right?: number;
  left?: number;
  mobileLeft?: number;
  mobileRight?: number;
}

export const PopoverBody = styled(animated.div)<IPropTypes>`
  position: absolute;
  background: #ffffff;
  width: ${(props) => (props.width ? `${props.width}px` : null)};
  top: ${(props) => (props.top ? `${props.top}px` : 0)};
  left: ${(props) => (props.left ? `${props.left}px` : null)};
  right: ${(props) => (props.right ? `${props.right}px` : null)};
  box-shadow: 0px 10px 50px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  z-index: 1001;

  @media (max-width: 767px) {
    left: ${(props) => (props.mobileLeft ? `${props.mobileLeft}px` : null)};
    right: ${(props) => (props.mobileRight ? `${props.mobileRight}px` : null)};
  }
`;
